/*
 * Copyright 2015 Jäger Nicolas <jagernicolas@legtux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *                                                                                  
 */
 
#include "SimulationSpheresDures"
#include <iomanip>

Particule::Particule()
: position(2)
, vitesse(2)
, rouge(0.)
, bleu(0.)
{
}

SimulationSpheresDures::SimulationSpheresDures( const uint M
                                              , const uint n
                                              , const double_t l
                                              , const double_t h
                                              , const double_t m
                                              , const double_t r
                                              , const double_t E  
                                              , const double_t e
                                              )
: maxIteration(M)
, nombreDeParticules(n)
, largeurBoite(std::fabs(l))
, hauteurBoite(std::fabs(h))
, masse(std::fabs(m))
, rayon(std::fabs(r))
, energie(std::fabs(E))
, epsilon(std::fabs(e))
, listeDesParticules(n)
, tProchaineCollision(std::pow(2,500))
, iteration(0)
, entropie(0.)
, kB(1.) // Boltzmann's constant set to 1. (it`s not revelant)
{

  // calcul de la deviation standard utilisee pour les vitesses des particules
  stdev = std::sqrt( 2.*energie/(masse*2.*nombreDeParticules) ) ;
  
  entropieTxt.open("entropie.txt");
  
  if ( !entropieTxt.is_open() )
  {
    std::cerr << "(EE) erreur lors de la creation du fichier!" << std::endl;
    
    exit(0);
    
  }

}

SimulationSpheresDures::~SimulationSpheresDures( void )
{
  entropieTxt.close();

}

void
SimulationSpheresDures::PlacerLesParticulesRougesEtBleues( const double_t separation )
{
  arma::vec boite = { largeurBoite, hauteurBoite };
  for ( auto &&particule : listeDesParticules )
  {
    // `arma::randu<arma::vec>(2)`, genere aleatoirement un vecteur 2D contenant des elements compris
    // entre [0,1]. `arma::randu` designe une distribution uniforme.
    arma::vec uniforme = arma::randu<arma::vec>(2);
    
    // on limite l'espace de la boite (on la reduit de 2%) dans laquelle on pose nos particules afin d`eviter des problemes de bords
    uniforme = ( uniforme * 0.98 ) + 0.01;
    
    ( uniforme[1] > separation ) ? particule.bleu = 1.0 : particule.rouge = 1.0 ;

    // comme la boite n`est pas forcement de dim 1x1, on multiplie par les dimensions de la boite.
    // note: `%` signifie qu`on multiplie element par element.
    particule.position = uniforme % boite;
    
    // on transforme ~30% des particules rouges en bleues pour ne pas avoir un systeme avec des particules uniformement distribuees
    if ( particule.rouge == 1.0 && ((double) rand() / (RAND_MAX)) < 0.31 )
    {
      particule.rouge = 0.;
            
      particule.bleu = 1.;
      
      // on deplace la particule dans la zone reservee aux particules bleues
      particule.position[1] = ( (uniforme[1]/separation)*(1.-separation)+separation ) * hauteurBoite;
      
    }
    
  }
  
  // on enregistre la position initiale de notre particule associee a la marche aleatoire
  trajectoire.push_back( listeDesParticules[0].position );

}

void
SimulationSpheresDures::PlacerUniquementLesParticulesRouges( const double_t limite )
{
  arma::vec boite = { largeurBoite, hauteurBoite };
  for ( auto &&particule : listeDesParticules )
  {
    // `arma::randu<arma::vec>(2)`, genere aleatoirement un vecteur 2D contenant des elements compris
    // entre [0,1]. `arma::randu` designe une distribution uniforme.
    arma::vec uniforme = arma::randu<arma::vec>(2);
    
    // on limite l`espace de la boite (on la reduit de 2%) dans laquelle on pose nos particules afin d`eviter des problemes de bords
    uniforme = ( uniforme * 0.98 ) + 0.01;
    
    // on limite l`espace de la boite selon le choix de l'utilisateur
    uniforme *= limite;
    
    particule.rouge = 1.0 ;

    // comme la boite n`est pas forcement de dim 1x1, on multiplie par les dimensions de la boite.
    // note: `%` signifie qu`on multiplie element par element.
    particule.position = uniforme % boite;
    
  }
  
  // on enregistre la position initiale de notre particule associee a la marche aleatoire
  trajectoire.push_back( listeDesParticules[0].position );
  
  // on change la couleur de la particule que l`on va suivre
  listeDesParticules[0].bleu  = 1.;
  listeDesParticules[0].rouge = 0.;

}

void
SimulationSpheresDures::FixerLesVitessesInitiales( void )
{
	double_t sum = 0;
	
	// `arma::randn<arma::vec>(2)`, genere aleatoirement un vecteur 2D en suivant une distribution
	// normale. Centree en 0 et de variance 1.
	for ( auto &&particule : listeDesParticules )
	{
    
    if ( particule.bleu == 1.0 )
    {
      // les particules bleues doivent etre deposees au fond sans vitesse initiales
      particule.vitesse = { 0. , 0. };
      
      continue;
      
    }
    
    particule.vitesse = arma::randn<arma::vec>(2);
    
    sum += arma::norm( particule.vitesse ); // `norm` vs `normalise` :: think about it : n`norm` should be the norm, so it`s should be good there!
    
	}
  
  sum = std::sqrt( sum );
  
  // on normalise nos vitesses de sorte a ce que l`on respecte la physique $sqrt{2 m E}$.
  for ( auto &&particule : listeDesParticules )
	{
    particule.vitesse *=  (stdev/sum);
    
  }
	
}

void 
SimulationSpheresDures::CollisionParticulesParticules(void)
{
  double_t tcollision = std::pow(2,500);
  
  for ( auto &&particule1 : listeDesParticules ) // on calcul pour chaque paire de
  for ( auto &&particule2 : listeDesParticules ) // particules le temps de tcollision.
  {

    const arma::vec deltaPosition = particule1.position - particule2.position;
    
    const arma::vec deltaVitesse = particule1.vitesse - particule2.vitesse;
    
    const double_t dotPosVit = arma::dot(deltaPosition,deltaVitesse);
    
    const double_t dotPosPos = arma::dot(deltaPosition,deltaPosition);
    
    const double_t dotVitVit = arma::dot(deltaVitesse,deltaVitesse);
    
    const double_t argRacine = dotPosVit*dotPosVit - std::fabs( dotVitVit ) * ( std::fabs( dotPosPos ) - 4.*rayon*rayon);
    
    // Les particules doivent s`approcher, `dotPosVit` < 0
    // et `racineCarre` doit etre strictement > 0
    if ( argRacine > epsilon && dotPosVit < 0. )
    {
      tcollision =  - ( dotPosVit + sqrt(argRacine) ) / dotVitVit;
     
      // on elimine la solution precedente si `tcollision` < 0
      if ( tcollision < 0. )
      {
        tcollision = - ( dotPosVit - sqrt(argRacine) ) / dotVitVit;
        
      }
      
      if ( tcollision < particule1.tProchaineCollision )
      {
        particule1.tProchaineCollision = tcollision;
        
        particule1.particuleAPercuter = &particule2;
        
      }
      
      if ( tcollision < particule2.tProchaineCollision )
      {
        particule2.tProchaineCollision = tcollision;
        
        particule2.particuleAPercuter = &particule1;
        
      }
     
    }
    else
    {
      tcollision = std::pow(2, 500);
      
    }
    
    if ( tcollision < tProchaineCollision )
    {
      tProchaineCollision = tcollision;
    
    }
    
  }
  
}

void
SimulationSpheresDures::CollisionParticulesMurs(void)
{
 
  for ( auto &&particule : listeDesParticules )
  {
    double_t tcollision = ( 0. + rayon - particule.position[0] ) / particule.vitesse[0];
    
    if ( tcollision > epsilon && tcollision <= tProchaineCollision && particule.vitesse[0] < 0 )
    {
      tProchaineCollision = tcollision;
      
      particule.tProchaineCollision = tcollision;
      
      particule.murX = 0.;
      
    }
    
    tcollision = ( largeurBoite - rayon - particule.position[0] ) / particule.vitesse[0];
    
    if ( tcollision > epsilon && tcollision <= tProchaineCollision && particule.vitesse[0] > 0 )
    {
      tProchaineCollision = tcollision;
      
      particule.tProchaineCollision = tcollision;
      
      particule.murX = largeurBoite;
      
    }
    
    tcollision = ( 0. + rayon - particule.position[1] ) / particule.vitesse[1];
    
    if ( tcollision > epsilon && tcollision <= tProchaineCollision && particule.vitesse[1] < 0 )
    {
      tProchaineCollision = tcollision;
      
      particule.tProchaineCollision = tcollision;
      
      particule.murY = 0.;
      
    }
    
    tcollision = ( hauteurBoite - rayon - particule.position[1] ) / particule.vitesse[1];
    
    if ( tcollision > epsilon && tcollision <= tProchaineCollision && particule.vitesse[1] > 0 )
    {
      tProchaineCollision = tcollision;
      
      particule.tProchaineCollision = tcollision;
      
      particule.murY = hauteurBoite;
      
    }    
    
  }
  
}

void
SimulationSpheresDures::BougerParticules(void)
{
  
  double_t dt = 1.0;
  
  for ( double_t t = 0 ; t < tProchaineCollision-dt ; t+= dt)
  {
    // on creer une image de l`etat
    CreerImageSysteme();
    
    // on creer une image de la marche aleatoire de la premiere particule
    CreerImageMarcheAleatoire();
    
    // caclul de l`entropie
    CalculEntropie();
    
    for ( auto &&particule : listeDesParticules )
    {
      particule.position += ( dt * particule.vitesse ) ;

    }
    
    trajectoire.push_back( listeDesParticules[0].position );
    
    entropieTxt << iteration << "  " << entropie << std::endl;
    
    iteration++;
    
  }
  
}

void
SimulationSpheresDures::ReinitialiserCertainesVariables(void)
{
  tProchaineCollision = std::pow(2, 500);
  
  entropie = 0.;
  
  for ( auto &&particule : listeDesParticules )
  {
    particule.tProchaineCollision = std::pow(2,500);
    
    particule.particuleAPercuter = nullptr;
    
    particule.murX = -1.0;
    
    particule.murY = -1.0;
    
  }
  
}

void
SimulationSpheresDures::Collisionner( void )
{
  for ( auto &&particule : listeDesParticules )
  {
    if ( particule.tProchaineCollision == tProchaineCollision )
    {
      if ( particule.particuleAPercuter )
      {
        const arma::vec deltaPosition = particule.position - particule.particuleAPercuter->position;
        
        const arma::vec deltaVitesse = particule.vitesse - particule.particuleAPercuter->vitesse;
        
        const double_t dotPosPos = arma::dot( deltaPosition, deltaPosition );
        
        const arma::vec deltaPositionNormalise = arma::normalise( deltaPosition ); // `norm` vs `normalise`, here we need a vector, so `normalise` sounds good...
        
        const arma::vec variationVitesse = deltaPositionNormalise * arma::dot( deltaVitesse, deltaPositionNormalise );
        
        particule.vitesse -= variationVitesse;
        
        particule.particuleAPercuter->vitesse += variationVitesse;
        
        // on efface le pointeur suivant pour eviter de traiter deux fois la meme collision en intervertissant les particules
        particule.particuleAPercuter->particuleAPercuter = nullptr;
        
      }
      else if ( particule.murX > -1.0 )
      {
        particule.vitesse[0] *= -1;
        
	  }
	  else if ( particule.murY > -1.0 )
	  {
        particule.vitesse[1] *= -1;
        
      }

    }
    
  }
  
}

void
SimulationSpheresDures::CreerImageSysteme( void )
{
  const double_t scallingFactor = 40.;
  
  Cairo::RefPtr<Cairo::ImageSurface> surface = Cairo::ImageSurface::create( Cairo::FORMAT_ARGB32
                                                                          , 10*scallingFactor+40
                                                                          , 20*scallingFactor+40 
                                                                          );

  Cairo::RefPtr<Cairo::Context> cr = Cairo::Context::create( surface );

  // on fixe le fond noir
  cr->set_source_rgb(1., 1., 1.);
  cr->paint();

  // on dessine le contour de la boite
  cr->set_source_rgb( 0., 0., 0. );
  cr->set_line_width( 1.0 ); // make the line wider
  cr->rectangle( 20.-1.
               , 20.-1.
               , 10.*scallingFactor+1.
               , 20.*scallingFactor+1.
               );
  cr->stroke();

  
  // on dessine les particules:
  for ( auto &&particule : listeDesParticules )
  {
  
    cr->set_source_rgb( particule.rouge, 0., particule.bleu );
  
    cr->arc( 20.+particule.position[0] * scallingFactor
           , 20.+particule.position[1] * scallingFactor
           , scallingFactor * rayon
           , 0., 2. * M_PI 
           );
           
    cr->fill();
   
  }

  std::stringstream buffer;
  buffer.fill('0');
  buffer << std::setfill('0') << std::setw(4) << iteration;


  std::string filename = "systeme" + buffer.str() + ".png";
  surface->write_to_png( filename );

}

void
SimulationSpheresDures::CreerImageMarcheAleatoire( void )
{
  const double_t scallingFactor = 40.;
  
  Cairo::RefPtr<Cairo::ImageSurface> surface = Cairo::ImageSurface::create( Cairo::FORMAT_ARGB32
                                                                          , 10*scallingFactor+40
                                                                          , 20*scallingFactor+40 
                                                                          );

  Cairo::RefPtr<Cairo::Context> cr = Cairo::Context::create( surface );

  // on fixe le fond noir
  cr->set_source_rgb(1., 1., 1.);
  cr->paint();

  // on dessine le contour de la boite
  cr->set_source_rgb( 0., 0., 0. );
  cr->set_line_width( 1.0 );
  cr->rectangle( 20.-1.
               , 20.-1.
               , 10.*scallingFactor+1.
               , 20.*scallingFactor+1.
               );
  cr->stroke();

  cr->set_source_rgb(0., 0., 0.);

  cr->set_line_width(1.0);
  
  // on dessine le trajet de la particules:
  for ( auto &&position : trajectoire )
  {  
    cr->line_to( 20+position[0]*scallingFactor, 20+position[1]*scallingFactor );
    
  }

  cr->stroke();
 

  std::stringstream buffer;
  buffer.fill('0');
  buffer << std::setfill('0') << std::setw(4) << iteration;


  std::string filename = "marcheAleatoire" + buffer.str() + ".png";
  surface->write_to_png( filename );

}

void
SimulationSpheresDures::CalculEntropie( void )
{
  
  const double_t cellSize = 0.5;
  
  std::vector<double_t> Pij( largeurBoite*hauteurBoite / (cellSize*cellSize) );
  
  uint ij = 0;
  
  for ( double_t i = 0 ; i < largeurBoite ; i+= cellSize )
  for ( double_t j = 0 ; j < hauteurBoite ; j+= cellSize )
  {
    for ( auto &&particule : listeDesParticules )
    {
      const double_t x = particule.position[0]-i ;
      const double_t y = particule.position[1]-j ;
      
      if ( x < cellSize && x > 0. && y < cellSize && y > 0. )
      {
        Pij[ij]++;
        
      }
      
    }
    
    ij++;
   
  }
  
  
  for ( auto p : Pij )
  {
    
    
    if ( p > 0 )
    {
      p /= static_cast<double_t>(nombreDeParticules);  
      
      entropie -= kB * p * std::log(p);
      
    }
    
  }
    
}
