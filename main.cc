/*
 * Copyright 2015 Jäger Nicolas <jagernicolas@legtux.org>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 *
 */
 
#include "SimulationSpheresDures"
#include <iostream>

// fonction main
int main(int argc, char **argv)
{
  // on initialise la simulation.
  SimulationSpheresDures* simulation = new SimulationSpheresDures(5000, 2000,10,20,1,0.03,500,0.000000000001);

  // on place les particules aleatoirement dans la boite.
  //~ simulation->PlacerLesParticulesRougesEtBleues(0.90);
  simulation->PlacerUniquementLesParticulesRouges(0.3);
  
  // on fixe les vitesses initiales aleatoirement.
  simulation->FixerLesVitessesInitiales();
  
  // boucle de simulation
  while ( simulation->continuer() )
  {
    simulation->ReinitialiserCertainesVariables();
    
    simulation->CollisionParticulesParticules();
    
    // calculs des prochaines collisions particules-murs
    simulation->CollisionParticulesMurs();
    
    // deplacer les particules jusqu`a la prochaine collision
    simulation->BougerParticules();
    
    // gerer la/les collision/s
    simulation->Collisionner();
    
  }
  

  delete simulation;

  exit(0);
}
