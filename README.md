Licence :
=========
  Copyright 2015 Jäger Nicolas <jagernicolas@legtux.org>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

Résumé :
========
  Code source d'une simulation de Sphères dures. La simulation peut 
  générer un mélange de deux types de particule, ou bien un type, mais
  localisé dans une petite portion de la boite. La simulation calcul à
  chaque itération l'entropie de la boîte, elle dessine la position de
  chaque particule dans le système dans une image et dessine à chaque
  itération la trajectoire de la première particule (marche aléatoire).
  Les images sortent en format png. Vous pouvez les convertir en video
  par exemple en utilisant ffmpeg:

  $ ffmpeg -framerate 30 -pattern_type glob -i 'systeme*.png' -c:v libx264 systeme.mp4

  
Dépendances :
==============
  - cairomm
  - armadillo
  - cmake

Compiler :
==========
  $ cmake .

  $ make

bug connu :
============
  - Alfred the beetle
